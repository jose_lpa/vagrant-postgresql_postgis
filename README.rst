PostgreSQL/PostGIS Vagrant VM
=============================

Debian 7.6.0 VirtualBox VM running a PostgreSQL 9.1 database with PostGIS 2.1.5.


Requirements
------------

- A `VirtualBox <https://www.virtualbox.org/>`_ setup.
- `Ansible <http://www.ansible.com/>`_ IT automation software.
- And, of course, `Vagrant <http://www.vagrantup.com>`_.


Usage
-----

1. Clone this repo: ``git clone git@bitbucket.org:jose_lpa/vagrant-postgresql_postgis.git``.
2. Step into the root directory: ``cd vagrant-postgresql_postgis``.
3. Raise the VM: ``vagrant up``.

Vagrant will download the VM box if necessary, raise the VM and provision it,
automatically. If anything goes wrong during the provisioning operations, they
can be triggered again with the command ``vagrant provision``.

You can stop the VM when you finish working with it with the command ``vagrant halt``.

The VM will be destroyed (thus installed software, DB and its contents removed)
if you use the command ``vagrant destroy``.


Availability
------------

The VM is NAT-configured to be available in the IP 192.168.0.3. Be warned of
this and change that IP in the ``Vagrantfile`` if it is already in use in your
network.

An empty database with PostGIS capabilities is already created for your own
convenience. The DB name is ``vagrant_gis_database``. You can create as many
databases as you want, once logged into the VM with the command ``vagrant ssh``.
The user ``vagrant`` and password ``vagrant`` gives access to PostgreSQL with
full permissions.

Known all this, the empty database can be reached from your host machine in the usual
``5432`` standard PostgreSQL port. Try this from your local shell:

    ``psql vagrant_gis_database -U vagrant -W -h 192.168.100.3``

You should be now logged into the database and able to perform any query or
PostgreSQL management command.
